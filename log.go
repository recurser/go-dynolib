package dyno

import (
  "fmt"
  "log"
  "log/syslog"
  "os"
)

/**
 * The level of logs to output. Should be one of "debug", "info" or "error".
 */
var logLevel string;

/**
 * Set up log destination.
 */
func LogInit() (*os.File) {
  logLevel = os.Getenv("LOG_LEVEL")
  logTo   := os.Getenv("LOG_TO")
  if logTo == "" {
    return nil
  }

  if logTo == "syslog" {
    logHandle, err := syslog.New(syslog.LOG_NOTICE, "dynomatic-collector-__EBS_ENV__")
    if err != nil {
      fmt.Printf("[ERR] Could not write to syslog")
    } else {
      log.SetOutput(logHandle)
    }

    return nil
  }

  logHandle, err := os.OpenFile(logTo, os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
  if err != nil {
    fmt.Printf("[ERR] Could not write to the log file '%s'", logTo)
    return nil
  }

  log.SetOutput(logHandle)

  if logLevel == "" {
    logLevel = "debug"
  }

  return logHandle
}

/**
 * Prints an error log message.
 */
func LogError(message string, args ...interface{}) {
  if logLevel == "debug" || logLevel == "info" || logLevel == "err" {
    logGeneric("[ERROR]", message, args...)
  }
}

/**
 * Prints an informational log message.
 */
func LogInfo(message string, args ...interface{}) {
  if logLevel == "debug" || logLevel == "info" {
    logGeneric("[INFO] ", message, args...)
  }
}

/**
 * Prints a debug log message.
 */
func LogDebug(message string, args ...interface{}) {
  if logLevel == "debug" {
    logGeneric("[DEBUG]", message, args...)
  }
}

/**
 * Prints a generic log message for the given log level.
 */
func logGeneric(level string, message string, args ...interface{}) {
  log.Printf(fmt.Sprintf("%s %s", level, message), args...)
}
