package dyno

import (
  "os"

  "github.com/joho/godotenv"
  "github.com/stvp/rollbar"
)

var appEnvironment string

/**
 * Returns the app environment (production / staging etc).
 */
func UtilityEnvironment() string {
  return appEnvironment;
}

/**
 * Initializes rollbar.
 */
func UtilityInitRollbar() {
  if UtilityIsProduction() {
    rollbar.Token       = os.Getenv("ROLLBAR_TOKEN")
    rollbar.Environment = appEnvironment
  }
}

/**
 * Returns true if we are in the production environment, false otherwise.
 */
func UtilityIsProduction() bool {
  if appEnvironment == "" {
    appEnvironment = os.Getenv("APP_ENVIRONMENT")
  }

  return (appEnvironment == "production")
}

/**
 * Loads the .env environment file if it exists.
 */
func UtilityLoadEnv() {
  if _, err := os.Stat(".env"); err == nil {
    godotenv.Load()
  } else {
    LogInfo("No .env was found - using global environment.")
  }
}

/**
 * Returns true if we should report to rollbar, false otherwise.
 */
func UtilityShouldReportError(err error) bool {
  return (err != nil) && rollbar.Token != ""
}
