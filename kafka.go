package dyno

import (
  "crypto/tls"
  "crypto/x509"

  "github.com/bsm/sarama-cluster"
  "github.com/Shopify/sarama"
)


/**
 * Instantiates the kafka cluster producer.
 */
func GetKafkaClusterConsumer(certificate string, privateKey string, authority string, brokerList []string, groupId string, topics []string) cluster.Consumer {
  config     := cluster.NewConfig()
  tlsConfig  := getKafkaCredentials(certificate, privateKey, authority)
  if tlsConfig != nil {
    config.Net.TLS.Config = tlsConfig
    config.Net.TLS.Enable = true
  }

  config.Consumer.Offsets.Initial   = sarama.OffsetOldest
  config.Group.Return.Notifications = true

  consumer, err := cluster.NewConsumer(brokerList, groupId, topics, config)
  if err != nil {
    LogError("Failed to start the Kafka cluster consumer:", err)
  }

  return *consumer
}


/**
 * Returns credentials for connecting to kafka.
 */
func getKafkaCredentials(certificate string, privateKey string, authority string) (t *tls.Config) {
  if certificate != "" && privateKey != "" && authority != "" {
    cert, err := tls.X509KeyPair([]byte(certificate), []byte(privateKey))
    if err != nil {
      LogError("Error loading Kafka credentials", err)
    }

    authorityPool := x509.NewCertPool()
    authorityPool.AppendCertsFromPEM([]byte(authority))

    t = &tls.Config{
      Certificates:       []tls.Certificate{cert},
      RootCAs:            authorityPool,
      InsecureSkipVerify: false,
    }
  }

  return t
}
